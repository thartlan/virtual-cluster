## virtual-cluster

### Overview

Connect clusters together under a single "master cluster" - across
regions and clouds. Pods scheduled onto a virtual kubelet node are forwarded
to the target cluster, and are processed by the kube-scheduler running in
that cluster to choose the actual node to run on.

![](img/virtual-multicluster.png)

The capacity of the virtual kublet (CPUs, memory, pods) is the sum of the capacities
of healthy nodes in the target cluster.

Alternatively, the capacity can be manually set using labels on the node itself,
which is necessary for autoscaling clusters where the current capacity is not
always equal to the maximum capacity.

A further overview with details on technical
restrictions can be read in [docs/overview.md](docs/overview.md).

### Example

Create configmap for kubeconfig for other cluster:
```
$ kubectl create ns nodes
$ kubectl -n nodes create configmap external-cluster-config --from-file=external-kubeconfig
```

Deploy virtual-cluster node:
```
$ kubectl create -f example/virtual-cluster-svcaccount.yaml
$ kubectl create -f example/virtual-cluster.yaml
```

Deploy a pod with the tolerance `"virtual-kubelet.io/provider"`:
```
$ kubectl create -f example/test-pod.yaml
```

Label the node to report a specific capacity:
```
$ kubectl label node cluster-name virtual-cluster/max-cpu=10 virtual-cluster/max-mem=20G virtual-cluster/max-pods=500

$ kubectl get nodes cluster-name --output=custom-columns='NAME:.metadata.name,CPU:.status.allocatable.cpu,MEM:.status.allocatable.memory'
NAME           CPU   MEM
cluster-name   10    20G
```
