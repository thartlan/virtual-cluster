FROM gcr.io/distroless/static

ADD virtual-cluster /virtual-cluster

CMD ["/virtual-cluster"]
