package main

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

const (
	labelMaxCPU  = "virtual-cluster/max-cpu"
	labelMaxMem  = "virtual-cluster/max-mem"
	labelMaxPods = "virtual-cluster/max-pods"
	labelMaxGPU  = "virtual-cluster/max-nvidia-gpu"

	resourceNvidiaGPU = corev1.ResourceName("nvidia.com/gpu")
)

type clusterCapacity struct {
	maxCPU       resource.Quantity
	maxMem       resource.Quantity
	maxPods      resource.Quantity
	maxNvidiaGPU resource.Quantity
}

func (c *clusterCapacity) Equals(other *clusterCapacity) bool {
	return c.maxCPU.Equal(other.maxCPU) && c.maxMem.Equal(other.maxMem) && c.maxPods.Equal(other.maxPods) && c.maxNvidiaGPU.Equal(other.maxNvidiaGPU)
}

func (c *clusterCapacity) String() string {
	return fmt.Sprintf("cpu=%s,mem=%s,pods=%s,nv-gpu=%s", &c.maxCPU, &c.maxMem, &c.maxPods, &c.maxNvidiaGPU)
}

func capacityFromNode(node *corev1.Node) (*clusterCapacity, error) {
	capacity := clusterCapacity{}
	for label, value := range node.ObjectMeta.Labels {
		switch label {
		case labelMaxCPU:
			resourceQuantity, err := resource.ParseQuantity(value)
			if err != nil {
				return nil, fmt.Errorf("could not parse CPU label: %v", err)
			}
			capacity.maxCPU = resourceQuantity
		case labelMaxMem:
			resourceQuantity, err := resource.ParseQuantity(value)
			if err != nil {
				return nil, fmt.Errorf("could not parse memory label: %v", err)
			}
			capacity.maxMem = resourceQuantity
		case labelMaxPods:
			resourceQuantity, err := resource.ParseQuantity(value)
			if err != nil {
				return nil, fmt.Errorf("could not parse pods label: %v", err)
			}
			capacity.maxPods = resourceQuantity
		case labelMaxGPU:
			resourceQuantity, err := resource.ParseQuantity(value)
			if err != nil {
				return nil, fmt.Errorf("could not parse nvidia gpu label: %v", err)
			}
			capacity.maxNvidiaGPU = resourceQuantity
		}
	}
	return &capacity, nil
}

func setCapacityOnNode(capacity *clusterCapacity, node *corev1.Node) {
	cpu := node.Status.Capacity[corev1.ResourceCPU]
	mem := node.Status.Capacity[corev1.ResourceMemory]
	pods := node.Status.Capacity[corev1.ResourcePods]
	nvidiaGPUs := node.Status.Capacity[resourceNvidiaGPU]
	if !capacity.maxCPU.IsZero() {
		cpu = capacity.maxCPU
	}
	if !capacity.maxMem.IsZero() {
		mem = capacity.maxMem
	}
	if !capacity.maxPods.IsZero() {
		pods = capacity.maxPods
	}
	if !capacity.maxNvidiaGPU.IsZero() {
		nvidiaGPUs = capacity.maxNvidiaGPU
	}
	node.Status.Capacity = corev1.ResourceList{
		corev1.ResourceCPU:    cpu,
		corev1.ResourceMemory: mem,
		corev1.ResourcePods:   pods,
		resourceNvidiaGPU:     nvidiaGPUs,
	}

	node.Status.Allocatable = corev1.ResourceList{
		corev1.ResourceCPU:    cpu,
		corev1.ResourceMemory: mem,
		corev1.ResourcePods:   pods,
		resourceNvidiaGPU:     nvidiaGPUs,
	}
}
